#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "imagem.hpp"

class Filtro : public Imagem {

public:
  Filtro();
  ~Filtro();

  void insercaoFiltro();
};

#endif
