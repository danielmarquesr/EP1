#ifndef MEDIA_HPP
#define MEDIA_HPP

#include "filtro.hpp"

class Media : public Filtro {
  unsigned char **red_media, **green_media, **blue_media;
public:
  Media();
  ~Media();
  unsigned char** getRed_media();
  void setRed_media(unsigned char **red_media);
  unsigned char** getGreen_media();
  void setGreen_media(unsigned char **green_media);
  unsigned char** getBlue_media();
  void setBlue_media(unsigned char **blue_media);

  void insercaoFiltro();
};

#endif
