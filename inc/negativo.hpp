#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include "filtro.hpp"

class Negativo : public Filtro {
  unsigned char **red_negativo, **green_negativo, **blue_negativo;
public:
  Negativo();
  ~Negativo();
  unsigned char** getRed_negativo();
  void setRed_negativo(unsigned char **red_negativo);
  unsigned char** getGreen_negativo();
  void setGreen_negativo(unsigned char **green_negativo);
  unsigned char** getBlue_negativo();
  void setBlue_negativo(unsigned char **blue_negativo);

  void insercaoFiltro();
};

#endif
