#include "polarizado.hpp"

Polarizado::Polarizado(){

}
Polarizado::~Polarizado(){

}
unsigned char** Polarizado::getRed_polarizado(){
  return red_polarizado;
}
void Polarizado::setRed_polarizado(unsigned char **red_polarizado){
  this->red_polarizado = red_polarizado;
}
unsigned char** Polarizado::getGreen_polarizado(){
  return green_polarizado;
}
void Polarizado::setGreen_polarizado(unsigned char **green_polarizado){
  this->green_polarizado = green_polarizado;
}
unsigned char** Polarizado::getBlue_polarizado(){
  return blue_polarizado;
}
void Polarizado::setBlue_polarizado(unsigned char **blue_polarizado){
  this->blue_polarizado = blue_polarizado;
}

void Polarizado::insercaoFiltro(){
  unsigned char max_cor;
  unsigned char **red, **green, **blue;

	red = getRed();
	green = getGreen();
	blue = getBlue();
  max_cor = getMaximo();

  unsigned char **aux_red = new unsigned char *[getLargura()];
  unsigned char **aux_green = new unsigned char *[getLargura()];
  unsigned char **aux_blue = new unsigned char *[getLargura()];

  for(int aux=0;aux < getAltura();aux++){
    aux_red[aux] = new unsigned char [getLargura()];
    aux_green[aux] = new unsigned char [getLargura()];
    aux_blue[aux] = new unsigned char [getLargura()];
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      aux_red[linha][coluna]= red[linha][coluna];
  		aux_green[linha][coluna] = green[linha][coluna];
  		aux_blue[linha][coluna] = blue[linha][coluna];
    }
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      if(aux_red[linha][coluna] < (max_cor/2))
        aux_red[linha][coluna] = 0;
      else
        aux_red[linha][coluna] = max_cor;
      if (aux_green[linha][coluna] < (max_cor/2)) {
        aux_green[linha][coluna] = 0;
      }
      else
        aux_green[linha][coluna] = max_cor;
      if (aux_blue[linha][coluna] < (max_cor/2)) {
        aux_blue[linha][coluna] = 0;
      }
      else
        aux_blue[linha][coluna] = max_cor;
    }
  }

  setRed_polarizado(aux_red);
  setGreen_polarizado(aux_green);
  setBlue_polarizado(aux_blue);

  gravarImagem(getRed_polarizado(), getGreen_polarizado(), getBlue_polarizado(), "filtroPolarizado");

}
