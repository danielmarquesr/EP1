#include "media.hpp"

typedef struct{
  unsigned int r;
  unsigned int g;
  unsigned int b;
}mat_somatorio;

char validaMascara(char *mascara){
  while(*mascara < '1' || *mascara > '3') {
    cout << "Digite uma opcao valida.\n\nOpcao: ";
    cin >> mascara;
  }
  return *mascara;
}

Media::Media(){

}
Media::~Media(){

}
unsigned char** Media::getRed_media(){
  return red_media;
}
void Media::setRed_media(unsigned char **red_media){
  this->red_media = red_media;
}
unsigned char** Media::getGreen_media(){
  return green_media;
}
void Media::setGreen_media(unsigned char **green_media){
  this->green_media = green_media;
}
unsigned char** Media::getBlue_media(){
  return blue_media;
}
void Media::setBlue_media(unsigned char **blue_media){
  this->blue_media = blue_media;
}

void Media::insercaoFiltro(){
  unsigned char **red, **green, **blue;
  mat_somatorio valor;
  int limite, divisor;
  char mascara;

  red = getRed();
  green = getGreen();
  blue = getBlue();

  unsigned char **aux_red = new unsigned char *[getLargura()];
  unsigned char **aux_green = new unsigned char *[getLargura()];
  unsigned char **aux_blue = new unsigned char *[getLargura()];

  for(int aux=0;aux < (getAltura());aux++){
    aux_red[aux] = new unsigned char [getLargura()];
    aux_green[aux] = new unsigned char [getLargura()];
    aux_blue[aux] = new unsigned char [getLargura()];
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      aux_red[linha][coluna]= red[linha][coluna];
  		aux_green[linha][coluna] = green[linha][coluna];
  		aux_blue[linha][coluna] = blue[linha][coluna];
    }
  }

  cout << "Mascara:\n1- 3x3\n2- 5x5\n3- 7x7\n\nOpcao: ";
  cin >> mascara;
  mascara = validaMascara(&mascara);
  switch(mascara) {
    case '1':{
      limite = 3;
      divisor = 9;
      break;
    }
    case '2':{
      limite = 5;
      divisor = 25;
      break;
    }
    case '3':{
      limite = 7;
      divisor = 49;
    }
  }

  limite /= 2;
  for(int linha = limite;linha < getAltura()-limite*2;linha++){
    for(int coluna = limite;coluna < getLargura()-limite*2;coluna++){
      valor.r = 0;
      valor.g = 0;
      valor.b = 0;
      for(int i = linha - limite;i <= linha + limite;i++){
        for(int j = coluna - limite;j <= coluna + limite;j++){
            valor.r += aux_red[i+limite][j+limite];
            valor.g += aux_green[i+limite][j+limite];
            valor.b += aux_blue[i+limite][j+limite];
        }
      }

      valor.r /= divisor;
      valor.g /= divisor;
      valor.b /= divisor;

      aux_red[linha][coluna]= valor.r;
  		aux_green[linha][coluna] = valor.g;
  		aux_blue[linha][coluna] = valor.b;
    }
  }

  setRed_media(aux_red);
  setGreen_media(aux_green);
  setBlue_media(aux_blue);

  gravarImagem(getRed_media(), getGreen_media(), getBlue_media(), "filtroMedia");

}
