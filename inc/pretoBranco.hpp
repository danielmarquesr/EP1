#ifndef PRETOBRANCO_HPP
#define PRETOBRANCO_HPP

#include "filtro.hpp"

class PretoBranco : public Filtro {
  unsigned char **red_preto_branco, **green_preto_branco, **blue_preto_branco;
public:
  PretoBranco();
   ~PretoBranco();
  unsigned char** getRed_preto_branco();
  void setRed_preto_branco(unsigned char **red_preto_branco);
  unsigned char** getGreen_preto_branco();
  void setGreen_preto_branco(unsigned char **green_preto_branco);
  unsigned char** getBlue_preto_branco();
  void setBlue_preto_branco(unsigned char **blue_preto_branco);

  void insercaoFiltro();

};

#endif
