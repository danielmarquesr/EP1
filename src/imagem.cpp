#include "imagem.hpp"
#define NUM_LINHAS 50

Imagem::Imagem() {
	formato = "";
	largura = 0;
	altura = 0;
	maximo = 0;
}
Imagem::~Imagem() {

}
string Imagem::getFormato() {
	return formato;
}
void Imagem::setFormato(string formato) {
	this->formato = formato;
}
int Imagem::getLargura() {
	return largura;
}
void Imagem::setLargura(int largura) {
	this->largura = largura;
}
int Imagem::getAltura() {
	return altura;
}
void Imagem::setAltura(int altura) {
	this-> altura = altura;
}
int Imagem::getMaximo() {
	return maximo;
}
void Imagem::setMaximo(int maximo) {
	this->maximo = maximo;
}
unsigned char** Imagem::getRed(){
	return red;
}
void Imagem::setRed(unsigned char **red){
	this->red = red;
}
unsigned char** Imagem::getGreen(){
	return green;
}
void Imagem::setGreen(unsigned char **green){
	this->green = green;
}
unsigned char** Imagem::getBlue(){
	return blue;
}
void Imagem::setBlue(unsigned char **blue){
this->blue = blue;
}

char Imagem::lerImagem(char erro_arquivo) {
	string diretorio, nomeImagem, linha, formato;
	int largura, altura, maximo, aux;
	char c;

  cout << "\nDigite o diretorio da imagem: ";
  cin >> diretorio;
	cout << "Digite o nome da imagem: ";
	cin >> nomeImagem;
	nomeImagem = diretorio + nomeImagem + ".ppm" ;
	cout << string(NUM_LINHAS, '\n');
	cout << "\nDiretorio: " << nomeImagem << "\n" << endl;
	ifstream arquivo(nomeImagem.c_str());
	if(!arquivo) {
		cout << "Erro na abertura do arquivo !\n" << endl;
		erro_arquivo = '1';
	}
	else {
		getline(arquivo,formato);
		if(formato != "P6") {
			cout << "Nao e formato P6\n" << endl;
			erro_arquivo = '1';
		}
		else {
			setFormato(formato);
			aux=0;
			do {
				getline(arquivo,linha);
				if(linha[0] != '#') {
					if(aux == 0)
						arquivo.seekg(-4,arquivo.cur);
					else
						arquivo.seekg(-8,arquivo.cur);
				}
				aux++;
			}while(linha[0] == '#');

			arquivo >> largura;
			setLargura(largura);
			arquivo >> altura;
			setAltura(altura);
			arquivo >> maximo;
			setMaximo(maximo);

			cout << "Dados da imagem:\n\n";
			cout << "Formato: " << getFormato() << endl;
			cout << "Largura: " << getLargura() << endl;
			cout << "Altura: " << getAltura() << endl;
			cout << "Maximo de cor: " << getMaximo() << "\n" << endl;
			getline(arquivo,linha);

			unsigned char **red = new unsigned char *[getLargura()];
			unsigned char **green = new unsigned char *[getLargura()];
			unsigned char **blue = new unsigned char *[getLargura()];

			for(int aux=0;aux < getAltura();aux++){
				red[aux] = new unsigned char [getLargura()];
				green[aux] = new unsigned char [getLargura()];
				blue[aux] = new unsigned char [getLargura()];
			}

			for(int linha = 0;linha < getAltura();linha++){
		    for(int coluna = 0;coluna < getLargura();coluna++){
					arquivo.get(c);
		      red[linha][coluna] = c;
					arquivo.get(c);
		      green[linha][coluna] = c;
					arquivo.get(c);
		      blue[linha][coluna] = c;
		    }
		  }
			setRed(red);
			setGreen(green);
			setBlue(blue);

			arquivo.close();
		}
	}
	return erro_arquivo;
}

void Imagem::gravarImagem(unsigned char **red, unsigned char **green, unsigned char **blue, string nomeFiltro){
	string diretorio;
	cout << "\nDigite o diretorio da nova imagem com filtro: ";
	cin >> diretorio;
	diretorio = diretorio + nomeFiltro + ".ppm";
	ofstream imagemFiltro(diretorio.c_str());
	if(!imagemFiltro){
		cout << "ERRO ao salvar imagem com filtro " << endl;
	}
	else {
		imagemFiltro << getFormato();
		imagemFiltro.put('\n');
		imagemFiltro << getLargura();
		imagemFiltro.put('\n');
		imagemFiltro << getAltura();
		imagemFiltro.put('\n');
		imagemFiltro << getMaximo();
		imagemFiltro.put('\n');

		for(int linha = 0;linha < getAltura();linha++){
			for(int coluna = 0;coluna < getLargura();coluna++){
				imagemFiltro << red[linha][coluna];
				imagemFiltro << green[linha][coluna];
				imagemFiltro << blue[linha][coluna];
			}
		}
		imagemFiltro.close();
		cout << string(NUM_LINHAS, '\n');
		cout << "Nova imagem com filtro salva.\n" << endl;
		cout << "Diretorio: " << diretorio << "\n" << endl;
	}
}
